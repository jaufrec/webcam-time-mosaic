#!/usr/bin/env python3
import os
from subprocess import call
import datetime
import argparse


def str_to_date(date_str):
    return datetime.datetime.strptime(date_str, '%Y-%m-%d')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Create dummy images to test related scripts')
    parser.add_argument('--date',
                        type=str_to_date,
                        default=datetime.datetime.today().date())
    parser.add_argument('--basedir', default='/var/www/html')
    parser.add_argument('--reldir', default='webcam')
    parser.add_argument('--size', default='1920x1080')
    args = parser.parse_args()
    size = args.size
    minutes_in_day = 60 * 24
    working_date = args.date

    base_dir = '/'.join([args.basedir,
                         args.reldir,
                         '{0:04d}'.format(working_date.year),
                         '{0:02d}'.format(working_date.month),
                         '{0:02d}'.format(working_date.day)])
    os.makedirs(base_dir, exist_ok=True)

    for i in range(0, minutes_in_day):
        faketime = datetime.timedelta(minutes=i)
        hours, remainder = divmod(faketime.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)
        filename = '{0:02d}:{1:02d}:{2:02d}.jpg'.format(
            hours, minutes, seconds)
        full_path = '/'.join([base_dir, filename])
        color_percent = (minutes_in_day - i) * 100 / minutes_in_day
        color = 'xc:hsb({0}%,100%,100%)'.format(color_percent)
        return_code = call(['convert', '-size', size, color, full_path])
        if return_code != 0:
            raise
