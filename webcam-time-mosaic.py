#!/usr/bin/env python3

from string import Template
import argparse
import csv
import datetime
import errno
import glob
import os
import pandas as pd
import pytz
import shutil
import subprocess
import sys
import tempfile


def get_best_file(timestamp, baserel_path):
    """Find the picture closest to the desired time.  Since this is the part that touches the
    original datestamped files, this is where timezones are handled.

    Returns the timestamp and file path, or None

    """

    # Convert all datetimes to a DST-invariant zone, to eliminate the DST shift.
    invar_tz = pytz.timezone('Etc/GMT+8')  # TODO: parameterize this
    local_tz = pytz.timezone('US/Pacific')  # TODO: parameterize this
    invar_input_date = invar_tz.localize(timestamp)
    if invar_input_date.minute < 30:
        minute_wildcard = '[0-2]?'
    else:
        minute_wildcard = '[3-5]?'

    localized_timestamp = invar_input_date.astimezone(local_tz).strftime(
        '%Y/%m/%d/%H')

    search_string = '{0}/{1}:{2}:??.jpg'.format(
        baserel_path,
        localized_timestamp,
        minute_wildcard)

    matches = sorted(glob.glob(search_string))

    if matches:
        for match in matches:
            valid = subprocess.call(['jpeginfo',
                                     '-c',
                                     match], stdout=subprocess.PIPE)
            if valid == 0:
                return (timestamp, match)
            else:
                continue

    return (None, None)


def import_cropfile(cropfile):
    # TODO: validate fields
    crop_data = []
    with open(cropfile, 'r') as f:
        reader = csv.DictReader(f)
        for line in reader:
            crop_data.append(tuple((line['startdate'], line['croparg'])))
    crop_data.sort(key=lambda tuple: tuple[0])
    return crop_data


def get_paths(working_date):
    result = {}
    working_year = '{0:04d}'.format(working_date.year)
    working_month = '{0:02d}'.format(working_date.month)
    working_day = '{0:02d}'.format(working_date.day)
    baserel_path = os.path.join(args.basedir,
                                args.reldir)
    month_path = os.path.join(baserel_path,
                              working_year,
                              working_month)
    result['month'] = month_path
    result['day'] = os.path.join(month_path,
                                 working_day)
    code_path = os.path.dirname(os.path.realpath(__file__))
    template_path = os.path.join(code_path, 'templates')
    result['day_thumb_template'] = os.path.join(template_path, 'day_thumb.html')
    result['month_thumb_template'] = os.path.join(template_path, 'month_thumb.html')
    result['homepage_template'] = os.path.join(template_path, 'homepage.html')

    date_iso = '-'.join([working_year, working_month, working_day])
    result['date_pretty'] = date_iso
    month_iso = '-'.join([working_year, working_month])
    result['month_pretty'] = month_iso  # TODO: replace with e.g., "Jan 2015"
    day_big_thumb_name = '{0}_big_thumb.jpg'.format(date_iso)
    day_thumb_name = '{0}_thumb.jpg'.format(date_iso)
    day_thumb_d_name = '{0}_thumb_dated.jpg'.format(date_iso)
    month_thumb_name = '{0}_thumb.jpg'.format(month_iso)
    month_thumb_d_name = '{0}_thumb_dated.jpg'.format(month_iso)
    homepage_index_name = 'homepage_thumb.html'
    homepage_thumb_name = 'homepage_thumb.jpg'
    homepage_thumb_d_name = 'homepage_thumb_d.jpg'
    day_index_name = '{0}.html'.format(date_iso)
    month_index_name = '{0}.html'.format(month_iso)

    result['baserel_path'] = baserel_path
    result['day_big_thumb'] = os.path.join(month_path, day_big_thumb_name)
    result['day_thumb'] = os.path.join(month_path, day_thumb_name)
    result['day_thumb_d'] = os.path.join(month_path, day_thumb_d_name)
    result['day_index'] = os.path.join(month_path, day_index_name)
    result['month_index'] = os.path.join(baserel_path,
                                         month_index_name)
    result['month_thumb'] = os.path.join(baserel_path, month_thumb_name)
    result['month_thumb_d'] = os.path.join(baserel_path, month_thumb_d_name)
    result['homepage_index'] = os.path.join(baserel_path, homepage_index_name)
    result['homepage_thumb'] = os.path.join(baserel_path, homepage_thumb_name)
    result['homepage_thumb_d'] = os.path.join(baserel_path, homepage_thumb_d_name)

    rel_base_url = '/'.join(['', args.reldir])
    result['rel_month_thumb_url'] = '/'.join([rel_base_url, month_thumb_name])
    result['rel_month_thumb_d_url'] = '/'.join([rel_base_url, month_thumb_d_name])
    rel_month_url = '/'.join([rel_base_url, working_year, working_month])
    result['rel_month_url'] = rel_month_url
    result['rel_day_url'] = '/'.join([rel_month_url, working_day])
    result['rel_day_thumb_url'] = '/'.join([rel_month_url, day_thumb_name])
    result['rel_day_thumb_d_url'] = '/'.join([rel_month_url, day_thumb_d_name])
    result['rel_homepage_thumb_url'] = '/'.join([rel_base_url, homepage_thumb_name])
    result['rel_homepage_thumb_d_url'] = '/'.join([rel_base_url, homepage_thumb_d_name])

    return result


def make_day_thumb(working_date):
    """Generates a one-day thumbnail from images in temp dir, including
high-res version, web version, and web display version with date
overlay"""
    paths = get_paths(working_date)
    crop_data = None
    temp_path = tempfile.mkdtemp()
    # TODO: just load cropfile once per run maybe?
    if args.cropfile:
        try:
            crop_data = import_cropfile(args.cropfile)
        except Exception as e:
            print("Could not load cropfile {0}.  Error {1}".
                  format(args.cropfile, e))
    if args.debug:
        print('DEBUG: temp dir is {0}'.format(temp_path))

    prepare_images(working_date, paths['baserel_path'], temp_path, crop_data)
    file_list = sorted(glob.glob(os.path.join(temp_path, '??????.jpg')))
    command = ['montage'] + file_list + ['-geometry', args.day_thumb_framesize,
                                         '-tile', '48x1',
                                         '-border', '0',
                                         paths['day_big_thumb']]
    if args.debug:
        print('DEBUG: command is {0}'.format(subprocess.list2cmdline(command)))
    try:
        # using os.system instead of subprocess because subprocess
        # returns a montage error or an errno 9.
        os.system(subprocess.list2cmdline(command))
    except Exception as e:
        attempt = subprocess.list2cmdline(command)
        print('On command {0}, error {1}'.format(attempt, e))
        sys.exit(-1)
    subprocess.run(['convert', paths['day_big_thumb'],
                    '-resize', args.day_thumb_geometry,
                    paths['day_thumb']])
    subprocess.check_output(
        ['convert', paths['day_thumb'],
         '-gravity', 'Center',
         '-pointsize', '18',
         '-fill', 'gray', '-annotate', '+1+1', paths['date_pretty'],
         '-fill', 'gray', '-annotate', '-1-1', paths['date_pretty'],
         '-fill', 'gray', '-annotate', '-1+1', paths['date_pretty'],
         '-fill', 'black', '-annotate', '+1-1', paths['date_pretty'],
         '-fill', 'white', '-annotate', '+0+0', paths['date_pretty'],
         paths['day_thumb_d']])
    day_thumb_html_template = open(paths['day_thumb_template'])
    source = Template(day_thumb_html_template.read())
    template_params = {
        'rel_day_url': paths['rel_day_url'],
        'rel_day_thumb_url': paths['rel_day_thumb_url'],
        'rel_day_thumb_d_url': paths['rel_day_thumb_d_url'],
        'date_pretty': paths['date_pretty']}
    sink = source.substitute(template_params)
    f = open(paths['day_index'], 'w')
    f.write(sink)
    f.close()
    if args.debug:
        print("DEBUG: temp dir is {0}".format(temp_path))
    else:
        try:
            shutil.rmtree(temp_path)  # delete directory
        except OSError as e:
            if e.errno != errno.ENOENT:
                raise


def make_month_thumb(working_date):
    """Generates a one-month thumbnail from daily thumbs"""
    paths = get_paths(working_date)
    file_list = sorted(glob.glob(os.path.join(paths['month'], '*_big_thumb.jpg')), reverse=True)  # noqa
    subprocess.check_output(['montage'] + file_list +
                            ['-geometry', '+0+0',
                             '-tile', '1x{0}'.format(len(file_list)),
                             '-border', '0',
                             paths['month_thumb']])
    subprocess.check_output(['mogrify',
                             '-resize', args.month_thumb_geometry,
                             paths['month_thumb']])
    subprocess.check_output(
        ['convert', paths['month_thumb'],
         '-gravity', 'Center',
         '-pointsize', '18',
         '-fill', 'gray', '-annotate', '+1+1', paths['month_pretty'],
         '-fill', 'gray', '-annotate', '-1-1', paths['month_pretty'],
         '-fill', 'gray', '-annotate', '-1+1', paths['month_pretty'],
         '-fill', 'black', '-annotate', '+1-1', paths['month_pretty'],
         '-fill', 'white', '-annotate', '+0+0', paths['month_pretty'],
         paths['month_thumb_d']])
    month_thumb_html_template = open(paths['month_thumb_template'])
    source = Template(month_thumb_html_template.read())
    template_params = {
        'rel_month_url': paths['rel_month_url'],
        'rel_month_thumb_url': paths['rel_month_thumb_url'],
        'rel_month_thumb_d_url': paths['rel_month_thumb_d_url'],
        'month_pretty': paths['month_pretty']}
    sink = source.substitute(template_params)
    f = open(paths['month_index'], 'w')
    f.write(sink)
    f.close()


def make_mosaic(start_date, end_date):
    if not start_date:
        # TODO: figure out the earliest start date from the files
        return
    mosaic_temp_path = os.path.join(args.basedir,
                                    'mosaic_temp.png')
    thumb_temp_path = os.path.join(args.basedir,
                                   'thumb_temp.png')
    mosaic_name = '{0}_mosaic_{1}_{2}.jpg'.format(
        args.reldir, start_date.date(), end_date.date())
    mosaic_path = os.path.join(args.basedir, mosaic_name)

    daterange = pd.date_range(start_date, end_date)
    i = 0
    for working_date in daterange:
        working_year = '{0:04d}'.format(working_date.year)
        working_month = '{0:02d}'.format(working_date.month)
        working_day = '{0:02d}'.format(working_date.day)
        date_iso = '-'.join([working_year, working_month, working_day])
        day_thumb_name = '{0}_big_thumb.jpg'.format(date_iso)
        day_thumb_path = os.path.join(args.basedir,
                                      args.reldir,
                                      working_year,
                                      working_month,
                                      day_thumb_name)
        if not os.path.isfile(day_thumb_path):
            print('Missing {0} thumb: {1}'.format(working_date, day_thumb_path))
            continue

        if i == 0:
            convert_command = 'convert {0}  -scale 25% {1}'.format(
                day_thumb_path, mosaic_temp_path)
            subprocess.check_output(convert_command,
                                    shell=True)
            i += 1
            print('Initiating, {0}'.format(working_date))
        else:
            convert_command = 'convert {0}  -scale 25% {1}'.format(
                day_thumb_path, thumb_temp_path)
            subprocess.check_output(convert_command,
                                    shell=True)

            concat_command = 'montage -mode concatenate -tile 1x {1} {0} {1}'.format(  # noqa
                thumb_temp_path, mosaic_temp_path)
            subprocess.check_output(concat_command,
                                    shell=True)
            print('Added {0}'.format(working_date))

    convert_command = 'convert {0} {1}'.format(
        mosaic_temp_path, mosaic_path)
    subprocess.check_output(convert_command,
                            shell=True)
    cleanup_command = 'rm {0} {1} {2}'.format(
                day_thumb_path, thumb_temp_path, mosaic_temp_path)
    subprocess.check_output(cleanup_command,
                            shell=True)


def prepare_images(input_date, baserel_path, temp_path, crop_data):
    """Generate cropped or scaled images for one day.
    Hard-coded to 1 picture every 30 min."""

    file_list = []
    start_time = datetime.datetime.combine(input_date,
                                           datetime.time(0, 0))

    end_time = datetime.datetime.combine(input_date,
                                         datetime.time(23, 30))

    timestamp_list = pd.date_range(start_time, end_time, freq='30min')
    for timestamp in timestamp_list:
        file_list.append(get_best_file(timestamp, baserel_path))

    crop_arg = None

    i = 0
    for file in file_list:
        out_path = os.path.join(temp_path, '{0:06d}.jpg'.format(i))
        if file[1] is None:
            command = ['convert',
                       '-size',
                       args.framesize,
                       'canvas:rgb(121,129,144)',
                       out_path]
        else:
            if crop_data:
                # return latest crop_data row that is earlier than working date
                for datum in crop_data:
                    if args.debug:
                        print('DEBUG: datum: {0}, timestamp: {1}'.format(datum[0], file[0]))
                    if datetime.datetime.strptime(datum[0],
                                                      '%Y-%m-%d %H:%M:%S') > file[0]:
                        break
                    crop_arg = datum[1]
            if crop_arg:
                crop_command = ['-crop', crop_arg]
            else:
                crop_command = []
            command = ['convert'] + crop_command + [
                       '-scale',
                       args.framesize,
                       file[1],
                       out_path]
        if args.debug:
            print('command {0}'.format(command))
        return_code = subprocess.call(command)
        if return_code != 0:
            print('Bad image: {0}'.format(file))
            subprocess.call(['rm', out_path])
        i += 1
        if args.debug and i % 10 == 0:
            print(i)


def str_to_date(date_str):
    return datetime.datetime.strptime(date_str, '%Y-%m-%d')


def update_homepage(working_date):
    """Make a smaller thumb, for the homepage, and update html"""

    paths = get_paths(working_date)
    subprocess.check_output(['convert', paths['day_big_thumb'],
                             '-resize', args.homepage_thumb_geometry,
                             paths['homepage_thumb']])
    subprocess.check_output(
        ['convert', paths['homepage_thumb'],
         '-resize', args.homepage_thumb_geometry,
         '-gravity', 'Center',
         '-pointsize', '18',
         '-fill', 'gray', '-annotate', '+1+1', paths['date_pretty'],
         '-fill', 'gray', '-annotate', '-1-1', paths['date_pretty'],
         '-fill', 'gray', '-annotate', '-1+1', paths['date_pretty'],
         '-fill', 'black', '-annotate', '+1-1', paths['date_pretty'],
         '-fill', 'white', '-annotate', '+0+0', paths['date_pretty'],
         paths['homepage_thumb_d']])
    homepage_html_template = open(paths['homepage_template'], 'r')
    source = Template(homepage_html_template.read())
    template_params = {
        'rel_day_url': paths['rel_day_url'],
        'rel_homepage_thumb_url': paths['rel_homepage_thumb_url'],
        'rel_homepage_thumb_d_url': paths['rel_homepage_thumb_d_url'],
        'date_pretty': paths['date_pretty']}
    sink = source.substitute(template_params)
    f = open(paths['homepage_index'], 'w')
    f.write(sink)
    f.close()


def yesterday():
    return datetime.datetime.today().date() - datetime.timedelta(days=1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='Generate a timelapse video from still frames.')
    parser.add_argument('actions', nargs='+',
                        help='Action(s) the script should take.\
                        Any of daythumb, monththumb, rethumb, homepage,\
                        mosaic')
    parser.add_argument('--basedir', default='/var/www/html',
                        help='absolute path of base directory')
    parser.add_argument('--cropfile',
                        help='csv file with fields startdate, croparg\
                        the script will use croparg on each frame for every\
                        day from startdate until the next startdate in the\
                        file.  croparg must be +x+y')
    parser.add_argument('--working-date',
                        help='Date to process.  Defaults to yesterday',
                        type=str_to_date,
                        default=yesterday())
    parser.add_argument('--debug',
                        help='Retain temp files, show extra debugging\
                        information',
                        action='store_true'),
    parser.add_argument('--framesize', default='1920x1080',
                        help='Size of raw frames used for thumb;\
                        scaled unless offset is specified, in which case\
                        scaled and cropped')
    parser.add_argument('--homepage-thumb-geometry', default='800x',
                        help='Size of thumbnail shown on homepage')
    parser.add_argument('--month-thumb-geometry', default='256x',
                        help='Imagemagick geometry of monthly thumbnail.')
    parser.add_argument('--start-date',
                        type=str_to_date,
                        help='Start date for batch operations',
                        default=None)
    parser.add_argument('--end-date',
                        type=str_to_date,
                        help='End date for batch operations.  Defaults to yesterday',
                        default=yesterday())
    parser.add_argument('--reldir', default='webcam',
                        help='directory name of images.')
    parser.add_argument('--day-thumb-framesize',
                        help='imagemagick geometry for individual frame.\
                        This can be bigger than day-thumb-geometry, \
                        so that smaller thumbnails can be displayed on the \
                        page but bigger thumbnails retained for future use,\
                        e.g., the mosaic.',
                        default='192x108+0+0')
    parser.add_argument('--day-thumb-geometry', default='x18',
                        help='Imagemagick geometry of daily thumbnail displayed.')
    args = parser.parse_args()
    working_date = args.working_date
    framerate = 30
    duration = 0
    minutes_in_day = 60 * 24

    if 'rethumb' in args.actions:
        daterange = pd.date_range(args.start_date, args.end_date)
        for re_date in daterange:
            make_day_thumb(re_date.date())
            print('Completed {0} day thumb.'.format(re_date.date()))
        monthrange = pd.date_range(args.start_date, args.end_date, freq='M')
        for re_month in monthrange:
            make_month_thumb(re_month.date())
            print('Completed {0} month thumb.'.format(re_month.date()))

    if 'daythumb' in args.actions:
        make_day_thumb(working_date)
        print('Completed {0} day thumb.'.format(working_date))

    if 'monththumb' in args.actions:
        make_month_thumb(working_date)
        print('Completed {0} month thumb.'.format(working_date))

    if 'homepage' in args.actions:
        update_homepage(working_date)
        print('Updated homepage for {0}'.format(working_date))

    if 'mosaic' in args.actions:
        make_mosaic(args.start_date, args.end_date)
