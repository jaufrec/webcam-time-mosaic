<html>
<head>
<title><?php include("title.txt") ?></title>
  <link rel="stylesheet" type="text/css" href="basic.css" media="all">
</head>
<body>
<div class="wrap">
  <div class="header">
    <?php if(file_exists("webcam/homepage_thumb.html"))
	     include("webcam/homepage_thumb.html");
	  else
	     print "Yesterday's pictures not available";
	  ?>
  </div>
  <div class="main">
    <?php if(file_exists("webcam/current.html"))
	     include("webcam/current.html");
	  else
	     print "Current picture not available";
	  ?>
    <?php if(file_exists("mosaic.jpg"))
	  print "<p><a href='mosaic.jpg'>Mosaic</a></p>"
	  ?>
  </div>
  <div class="sidebar">
    <?php
       $index_files=glob("webcam/????-??.html");
       if( is_array($index_files) && count($index_files) > 0 )  {
    foreach ( array_reverse($index_files) as $name) {
    include($name);
          }
       }
       else
         print "No history available";
?>
  </div>
</div>

<div class="preload">
</div>
