# webcam-time-mosaic
Webcam-time-mosaic makes a mosaic of pictures of the same view over time, one picture every 30 minutes, one row per day.

![Image of monthly mosaic, 30 strips showing 48 tiny rectangular pictures each, from very dark to clouds to blue sky to dark.](https://gitlab.com/jaufrec/webcam-time-mosaic/raw/master/webcam_month.jpg)

## Why you would use this
Most picture timelapse scripts and software create timelapse movies.  This script instead creates a picture, a mosaic over time.  Use this script if you want to see that, and if you want to automatic all aspects of the process, from collecting pictures 24/7 to updating a webpage with the latest picture and keeping the mosaic current.

### Features
1. Scripts for capturing images from uvccapture, gphoto2, Capture, or common IP cameras that load timestamped images to a directory.
1. Auto-updating home page, with navigation to months, days, and individual photos, with datestamps on rollover.
1. Automatic cropping from a logbook, so that variations in camera placement can be compensated for
1. Generating a mosaic (single jpg) for any date range.

### Compatibility
1. Linux.  Developed and tested on Ubuntu.
1. Prerequisites: bash, imagemagick, jpeginfo, jpegpixi, python3
1. Webserver with PHP

## Usage

### Image capture
webcam-time-mosaic requires a fresh supply of images, every half hour.  These could come from any of several sources:
* https://sourceforge.net/projects/capture/ for Canon PowerShot
* uvccapture for USB cameras plugged into the host computer
* gphoto2 for many camera models
* Any system that can regularly deposit a file in a local directory.  This script was developed in part with HikVision cameras, which can FTP to a fixed location.  Note, however that HikVision cameras connected to the internet are essentially unsecurable.

Set up the capture script so that is capturing at least one image every 30 minutes, from one of these sources, and storing it within the webcam-time-mosaic file structure.

#### Cron job for image capture from a directory
```
#  m h  dom mon dow   command
1  *    *   *   *      ~/webcam-time-mosaic/capture.sh -d /var/www/html  -r webcam -i /home/webcam
3  *    *   *   *      ~/webcam-time-mosaic/capture.sh -d /var/www/html  -r webcam -i /home/webcam
31 *    *   *   *      ~/webcam-time-mosaic/capture.sh -d /var/www/html  -r webcam -i /home/webcam
33 *    *   *   *      ~/webcam-time-mosaic/capture.sh -d /var/www/html  -r webcam -i /home/webcam
```

### Website setup
1. Set up a webserver with PHP that serves `/var/www/html` as root.
1. Copy the php, png, and txt files from the repository to the webserver root directory.
  1. The user that runs the scripts should have permission to write to a directory on the webserver.  One way to do this on Linux is `usermod -a -G www-data <username`.
  1. If the webcam directory name is not `webcam`, replace all instances of `webcam` in `index.php` with the new name.

### Mosaic regeneration
1. Create a cron job to generate the daily mosaic and update the monthly mosaic every night, after midnight.
1. If the usable portion of the picture varies over time, you can specify which portion to use for which time period by creating cropfile.csv

#### Example cropfile.csv
```startdate,croparg
2018-08-03 12:45:00,689x388+870+396
2018-08-02 08:45:00,640x360+859+377
2018-05-13 16:45:00,767x431+585+279
```

#### Cron job for nightly update of mosaics
```
# midnight update
5 0    *   *   *      python3 ~/webcam-time-mosaic/webcam-time-mosaic.py daythumb monththumb homepage --framesize 640x360 --cropfile ~/cropfile.csv
```

### Regenerate daily and monthly mosaics for an arbitrary period of time
```python3 ~/webcam-time-mosaic/webcam-time-mosaic.py rethumb --framesize 640x360 --cropfile ~/cropfile.csv \
    --start-date 2017-08-02 --end-date 2018-08-03```

### Making printable mosaics
```python3 ~/webcam-time-mosaic/webcam-time-mosaic.py mosaic --framesize 640x360 --start-date 2016-12-24 --end-date 2017-12-20```

## Design
![Image of homepage, with a single current photo, a timestamp, and a mosaic of months on the right side.](https://gitlab.com/jaufrec/webcam-time-mosaic/raw/master/webcam_homepage_overlay.jpg)

All files and directories in this structure are automatically created by the scripts unless otherwise noted.
If more than one camera is used, the
current.html include line can be duplicated.

The directory structure will be:
```
File or directory                             Created by
------------------------------------------   -------------------------
/ 
 index.php (provided with webcam)             <- manual copy
 index_month.php (provided with webcam)       <- manual copy
 index_day.php (provided with webcam)         <- manual copy
 title.txt (provided with webcam - replace this with your title)
 basic.css (provided with webcam)             <- manual copy
 webcam/                                      <- capture.sh
        current.jpg                           <- capture.sh
        current.html                          <- capture.sh
        YYYY-MM_index.html                    <- webcam-time-mosaic
        YYYY-MM_thumb.jpg                     <- webcam-time-mosaic
        YYYY-MM_thumb_dated.jpg               <- webcam-time-mosaic
        YYYY/                                 <- capture.sh
             MM/                              <- capture.sh
                index.php                     <- capture.sh
                YYYY-MM-DD.html               <- webcam-time-mosaic
                YYYY-MM-DD_big_thumb.jpg      <- webcam-time-mosaic
                YYYY-MM-DD_thumb.jpg          <- webcam-time-mosaic
                YYYY-MM-DD_thumb_dated.jpg    <- webcam-time-mosaic
                DD/                           <- capture.sh
                   index.php                  <- capture.sh
                   HH:MM:SS.jpg               <- capture.sh
                   HH:MM:SS_thumb.jpg         <- capture.sh
                   HH:MM:SS.html              <- capture.sh
```

### Possible improvements

* Timezone corrections are hardcoded to Pacific time.  This should be generalized.
* Performance on mosaic is terrible.  Could instead mosaic by month and then glue together.
* There are gaps between mosaics in the homepage and annual pages.
* There really are a lot of paths; maybe this could be simplified.
* The details of the directory structure could be isolated in one area
* It's confusing to have both a working-date argument and start and end dates.
* The remaining bash scripts could be converted to Python
* daily_movie.sh generates a timelapse video, set to music.  It is broken as-is and could be either removed, or re-written in Python.
* The color used for missing times is hard-coded.  It could be autogenerated from current or previous average daily colors.
* Warnings and alternative cameras to make it more likely to capture without interruption
* The "yesterday" montage on the top of the home page should either be bigger or removed.

### Contributions
are welcome.

## A full year
![Image of annual mosaic, showing a blue area that curves narrow and wide with the seasons and black edges.](https://gitlab.com/jaufrec/webcam-time-mosaic/raw/master/mosaic.jpg)
